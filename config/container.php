<?php

declare(strict_types=1);

$container = new League\Container\Container();

require_once __DIR__ . '/../bootstrap.php';

use Doctrine\ORM\EntityManagerInterface;

$container->add(EntityManagerInterface::class, $entityManager)->addTag('doctrine.orm');

// Middleware section
use Pbrilius\C2cMvcPbgroupeu\Middleware\Authentication;
use Pbrilius\C2cMvcPbgroupeu\Middleware\Authorization;

$container->add(Authorization::class, function() use ($container) {
  $authorization = new Authorization($container->get('doctrine.orm')[0]);

  return $authorization;
})->addTag('authorization')->setShared();

$container->add(Authentication::class, function() use ($container) {
  $authentication = new  Authentication($container->get('doctrine.orm')[0]);

  return $authentication;
})->addTag('authentication')->setShared();

include_once __DIR__ . '/../monolog.php';
use Monolog\Logger;
$container->add(Logger::class, $logger)->addTag('logger');

use Twig\Environment;
include_once __DIR__ . '/../twig.php';
$container->add(Engine::class, $twig)->addTag('twig');
